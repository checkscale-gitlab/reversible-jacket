const proxyUrl = process.env.PROXY_URL || "http://localhost:3001";

export default {
  server: {
    proxy: {
      "/api": {
        target: proxyUrl,
        timeout: 2000,
        proxyTimeout: 2000
      },
    },
  },
};

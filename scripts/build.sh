#!/bin/bash
# This script builds the docker image.

ROOT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}")/.. && pwd)

cd $ROOT_DIR

docker build .